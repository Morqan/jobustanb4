// JOBS-SEARCH.HTML
//IF CHECKED AD <li class="jobs__criterias-list"> ELSE REMOVE <li class="jobs__criterias-list">

$(document).ready(function() {
    $(".form-check-input").change(function() {
      var name = $(this).attr("data-name");
      var value = $(this).attr("data-id");
      var $this = $(this);
      if ($this.is(':checked')) {
                    $(".jobs__criterias").append(
                        '<li class="jobs__criterias-list" id = "'+value+'">\n' +
                        '<button class="myBtn">'+ name +
                        '<a class="delete-criterias"><i class="flaticon-cancel"></i></a>\n' +
                        '</button>\n' +
                        '\n' +
                        '</li>'
                    )
      } else  {
        $('#jobs__criterias').find('#'+value).remove();    
        $('#jobs__criterias1').find('#'+value).remove();
        $('#jobs__criterias2').find('#'+value).remove();        
      }
    });
  });


 
//addCv
$(document).ready(function() {
    $("#blockIfCheked").change(function() {
      var $this = $(this);
      if ($this.is(':checked')) {
        $(".disabled").attr('style', 'display:block');
      } else  {
        $(".disabled").attr('style', 'display:none');
      }
    });
  });

  $(document).ready(function() {
    $("#d-noneIfCheked").change(function() {
      var $this = $(this);
      if ($this.is(':checked')) {
        $("#endDate").parent().attr('style', 'display:none');
      } else  {
        $("#endDate").parent().attr('style', 'display:block');
      }
    });
  });

  $(document).ready(function() {
    $("#minorDetails").change(function() {
      var $this = $(this);
      if ($this.is(':checked')) {
        $(".minor-details").attr('style', 'display:block');
        $( "#majorDetails" ).prop( "disabled", true );
      } else  {
        $(".minor-details").attr('style', 'display:none');
        $( "#majorDetails" ).prop( "disabled", false );
      }
    });
  });

  $(document).ready(function() {
    $("#majorDetails").change(function() {
      var $this = $(this);
      if ($this.is(':checked')) {
        $(".major-details").attr('style', 'display:block');
        $( "#minorDetails" ).prop( "disabled", true );
      } else  {
        $(".major-details").attr('style', 'display:none');
        $( "#minorDetails" ).prop( "disabled", false );
      }
    });
  });

  $(document).ready(function() {
    $("#stillStaying").change(function() {
      var $this = $(this);
      if ($this.is(':checked')) {
        $(".stillStaying").attr('style', 'display:none');
        $( "#dropout" ).prop( "disabled", true );
      } else  {
        $(".stillStaying").attr('style', 'display:block');
        $( "#dropout" ).prop( "disabled", false );
      }
    });
  });
  $(document).ready(function() {
    $("#dropout").change(function() {
      var $this = $(this);
      if ($this.is(':checked')) {
        $(".stillStaying").attr('style', 'display:none');
        $(".endDate").attr('style', 'display:none');
        $("#dropoutBlock").addClass("d-block");
        $( "#stillStaying" ).prop( "disabled", true );
      } else  {
        $(".stillStaying").attr('style', 'display:block');
        $(".endDate").attr('style', 'display:block');
        $("#dropoutBlock").removeClass("d-block");
        $( "#stillStaying" ).prop( "disabled", false );
      }
    });
  });

  $(document).ready(function() {
    $("#dropouthighSchool").change(function() {
      var $this = $(this);
      if ($this.is(':checked')) {
        $(".endDateSchool").attr('style', 'display:none');
        $( "#highSchoolContinue" ).prop( "disabled", true );
      } else  {
        $(".endDateSchool").attr('style', 'display:block');
        $( "#highSchoolContinue" ).prop( "disabled", false );
      }
    });
  });

  $(document).ready(function() {
    $("#advancedLevel").change(function() {
      var $this = $(this);
      var level = $(".js-example-placeholder-single30").select2();
      level.val("Advanced").trigger("change");
    
    });
  });

  $(document).ready(function() {
    $('#woman').click(function(){
      $(".ifWoman").addClass('d-none');
    });
    $('#man').click(function(){
      $(".ifWoman").removeClass('d-none');
    });
  });


  
  // $("input:radio").change(function () {
  //   if () {
  //     $("#otherGendInp").removeClass('d-none');
  //   }
  //   else {
  //     $("#otherGendInp").addClass('d-none');
  //   }
  // });
$(document).ready(function() {

    $(function () {
        $('[data-toggle="popover"]').popover()
      })
  
    // $(document).on("click",".applied__hintLink",function() {
    //    $(this).parent().find(".applied__hint").css("display", "block");
        
    // //    $(this).parent().children("applied__hint").css("display", "block");
    // });
    // $(document).on("click",".applied__hintClose",function() {
    //     $(this).parent().css("display", "none");
    // });

});
$('.all-courses__carousel').owlCarousel({
    loop:true,
    margin:10,
   
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
// function copyFropInput() {
//     var nameInput = document.getElementById('candidateNameInput');
//     var lastNameInput = document.getElementById('candidateLastNameInput');
//     var adressInput = document.getElementById('candidateAdressInput');
//     var mailInput = document.getElementById('candidateMailInput');
//     var siteInput = document.getElementById('candidateSiteInput');
//     var phoneInput = document.getElementById('candidatePhoneInput');
//     var birthdayInput = document.getElementById('candidateBirthdayInput');
//     var countryInput = document.getElementById("candidateCountryInput");
//     var countryValue = countryInput.options[countryInput.selectedIndex].textContent;
//     var sityInput = document.getElementById("candidateSityInput");
//     var sityValue = sityInput.options[sityInput.selectedIndex].textContent;
    
    

//     var nameParagraph = document.getElementById('candidateName');
//     var adressParagraph = document.getElementById('candidateAdress');
//     var mailParagraph = document.getElementById('candidateMail');
//     var siteParagraph = document.getElementById('candidateSite');
//     var phoneParagraph = document.getElementById('candidatePhone');
//     var birthdayParagraph = document.getElementById('candidateBirthday');
//     var countryParagraph = document.getElementById('candidateCountry');
//     var sityParagraph = document.getElementById('candidateSity');


//     nameParagraph.textContent = nameInput.value + " "+ lastNameInput.value;
//     adressParagraph.textContent = adressInput.value;
//     mailParagraph.textContent = mailInput.value;
//     siteParagraph.textContent = siteInput.value;
//     phoneParagraph.textContent = phoneInput.value;
//     birthdayParagraph.textContent = birthdayInput.value;
//     countryParagraph.textContent = countryValue;
//     sityParagraph.textContent = sityValue;

//     document.getElementById("resume__contact-form").style.display= "none";
//   }

  
$(document).ready(function() {    
    var textarea = $("#my_textarea");
    textarea.keydown(function(event) {
        var numbOfchars = textarea.val();
        var len = numbOfchars.length;
        $(".countcharacters--live").text(len);

    });
}); 

$(document).ready(function() {    
    var textarea = $("#my_textarea1");
    textarea.keydown(function(event) {
        var numbOfchars = textarea.val();
        var len = numbOfchars.length;
        $(".countcharacters--live1").text(len);

    });
}); 
$(document).ready(function() {    
    var textarea = $("#my_textarea2");
    textarea.keydown(function(event) {
        var numbOfchars = textarea.val();
        var len = numbOfchars.length;
        $(".countcharacters--live2").text(len);

    });
}); 
$(document).ready(function() {    
    var textarea = $("#my_textarea3");
    textarea.keydown(function(event) {
        var numbOfchars = textarea.val();
        var len = numbOfchars.length;
        $(".countcharacters--live3").text(len);

    });
}); 
$(document).ready(function() {    
    var textarea = $("#my_textarea4");
    textarea.keydown(function(event) {
        var numbOfchars = textarea.val();
        var len = numbOfchars.length;
        $(".countcharacters--live4").text(len);

    });
}); 
$(document).ready(function() {    
    var textarea = $("#my_textarea5");
    textarea.keydown(function(event) {
        var numbOfchars = textarea.val();
        var len = numbOfchars.length;
        $(".countcharacters--live5").text(len);

    });
}); 

$(document).ready(function() {    
    var textarea = $(".text__count");
    textarea.keydown(function(event) {
        var numbOfchars = textarea.val();
        var len = numbOfchars.length;
        $(".countcharacters--live6").text(len);

    });
}); 
$(document).ready(function() {    
    var textarea = $("#my_textarea6");
    textarea.keydown(function(event) {
        var numbOfchars = textarea.val();
        var len = numbOfchars.length;
        $(".countcharacters--live6").text(len);

    });
}); 
$(document).ready(function() {    
    var textarea = $("#my_textarea7");
    textarea.keydown(function(event) {
        var numbOfchars = textarea.val();
        var len = numbOfchars.length;
        $(".countcharacters--live7").text(len);

    });
}); 
$(document).ready(function() {    
    var textarea = $("#my_textarea8");
    textarea.keydown(function(event) {
        var numbOfchars = textarea.val();
        var len = numbOfchars.length;
        $(".countcharacters--live8").text(len);

    });
});

$(document).ready(function() {    
    var myInput = $(".letterTitle");
    myInput.keydown(function(event) {
        var numbOfchars = myInput.val();
        var len = numbOfchars.length;
        $(".countcharacters--live9").text(len);

    });
});
// Start upload preview image
// $(".gambar").attr("src", "https://user.gadjian.com/static/images/personnel_boy.png");
// var $uploadCrop,
//     tempFilename,
//     rawImg,
//     imageId;
// function readFile(input) {
//     if (input.files && input.files[0]) {
//         var reader = new FileReader();
//         reader.onload = function (e) {
//             $('.upload-demo').addClass('ready');
//             $('#cropImagePop').modal('show');
//             rawImg = e.target.result;
//         }
//         reader.readAsDataURL(input.files[0]);
//     }
//     else {
//         swal("Sorry - you're browser doesn't support the FileReader API");
//     }
// }

// $uploadCrop = $('#upload-demo').croppie({
//     viewport: {
//         width: 160,
//         height: 160,
//         type: 'circle'
//     },
//     enforceBoundary: false,
//     enableExif: true,
//     enableOrientation: true
// });
// $('#cropImagePop').on('shown.bs.modal', function () {
//     // alert('Shown pop');
//     $uploadCrop.croppie('bind', {
//         url: rawImg,
//         orientation: 1

//     }).then(function () {
//         console.log('jQuery bind complete');
//     });
//     $('.vanilla-rotate').on('click', function(ev) {
//         $uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
//     });
// });

// $('.item-img').on('change', function () {
//     imageId = $(this).data('id'); tempFilename = $(this).val();
//     $('#cancelCropBtn').data('id', imageId); readFile(this);
// });
// $('#cropImageBtn').on('click', function (ev) {
//     $uploadCrop.croppie('result', {
//         type: 'base64',
//         format: 'jpeg',
//         size: { width: 160, height: 160 }
//     }).then(function (resp) {
//         $('#item-img-output').attr('src', resp);
//         $('#cropImagePop').modal('hide');
//     });
// });
				// End upload preview image˝
//LANGUAGE DROPDOWN
var dropdownButton = document.querySelector('.lang-toggle');
var drMenuItem = document.querySelectorAll('.dropdown-item');
drMenuItem.forEach(function(item) {
    item.addEventListener("click", function(e) {
        dropdownButton.innerText = e.target.innerText;
    });
});
// Range





var acc = document.getElementsByClassName("acardion__btn");
var pluse = document.getElementsByClassName("acardion__plus");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function () {
    this.classList.toggle("acardion__active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";

    } else {

      panel.style.display = "block";
    }

  });
}


$(".acardion__btn").click(function () {
  $(this).find($(".acardion__btn--icon")).toggleClass('flaticon-plus').toggleClass('flaticon-minus-symbol acardion__btn--icon-white');
})
// JOBS-SEARCH.HTML

function myFunction() {
    var input, filter, ul, li, div, i, txtValue;
    input = document.getElementById("filterForCheckbox");
    filter = input.value.toUpperCase();
    ul = document.getElementById("checkbox-list");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
       var div = li[i].getElementsByClassName("form-check")[0];
       var txtValue = div.getAttribute('data-name');
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function myFunction1() {
    var input, filter, ul, li, div, i, txtValue;
    input = document.getElementById("filterForCheckbox1");
    filter = input.value.toUpperCase();
    ul = document.getElementById("checkbox-list1");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
       var div = li[i].getElementsByClassName("form-check")[0];
       var txtValue = div.getAttribute('data-name');
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function myFunction2() {
    var input, filter, ul, li, div, i, txtValue;
    input = document.getElementById("filterForCheckbox2");
    filter = input.value.toUpperCase();
    ul = document.getElementById("checkbox-list2");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
       var div = li[i].getElementsByClassName("form-check")[0];
       var txtValue = div.getAttribute('data-name');
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function myFunction3() {
    var input, filter, ul, li, div, i, txtValue;
    input = document.getElementById("filterForCheckbox3");
    filter = input.value.toUpperCase();
    ul = document.getElementById("checkbox-list3");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
       var div = li[i].getElementsByClassName("form-check")[0];
       var txtValue = div.getAttribute('data-name');
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
function myFunction4() {
    var input, filter, ul, li, div, i, txtValue;
    input = document.getElementById("filterForCheckbox4");
    filter = input.value.toUpperCase();
    ul = document.getElementById("checkbox-list4");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
       var div = li[i].getElementsByClassName("form-check")[0];
       var txtValue = div.getAttribute('data-name');
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function myFunction5() {
    var input, filter, ul, li, div, i, txtValue;
    input = document.getElementById("filterForCheckbox5");
    filter = input.value.toUpperCase();
    ul = document.getElementById("checkbox-list5");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
       var div = li[i].getElementsByClassName("form-check")[0];
       var txtValue = div.getAttribute('data-name');
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
function myFunction6() {
    var input, filter, ul, li, div, i, txtValue;
    input = document.getElementById("filterForCheckbox6");
    filter = input.value.toUpperCase();
    ul = document.getElementById("checkbox-list6");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
       var div = li[i].getElementsByClassName("form-check")[0];
       var txtValue = div.getAttribute('data-name');
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function myFunction7() {
    var input, filter, ul, li, div, i, txtValue;
    input = document.getElementById("filterForCheckbox7");
    filter = input.value.toUpperCase();
    ul = document.getElementById("checkbox-list7");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
       var div = li[i].getElementsByClassName("form-check")[0];
       var txtValue = div.getAttribute('data-name');
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function myFunction8() {
    var input, filter, ul, li, div, i, txtValue;
    input = document.getElementById("filterForCheckbox8");
    filter = input.value.toUpperCase();
    ul = document.getElementById("checkbox-list8");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
       var div = li[i].getElementsByClassName("form-check")[0];
       var txtValue = div.getAttribute('data-name');
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function myFunction9() {
    var input, filter, ul, li, div, i, txtValue;
    input = document.getElementById("filterForCheckbox9");
    filter = input.value.toUpperCase();
    ul = document.getElementById("checkbox-list9");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
       var div = li[i].getElementsByClassName("form-check")[0];
       var txtValue = div.getAttribute('data-name');
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function myFunctionCompany() {
    var input, filter, ul, li, div, i, txtValue;
    input = document.getElementById("filterForCheckbox10");
    filter = input.value.toUpperCase();
    ul = document.getElementById("checkbox-list10");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
       var div = li[i].getElementsByClassName("form-check")[0];
       var txtValue = div.getAttribute('data-name');
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}


  Inputmask({ alias: "datetime", inputFormat: "dd/mm/yyyy" }).mask(".qwerty");

  $("#phone").inputmask({"mask": "+999 (99) - 999 - 99 - 99"});


  $('#taxid').keypress(function(event){
    
    if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
    event.preventDefault();   
    } 
    if(this.value.length==10) {
      return false;
    }
});

function openNav() {
    document.getElementById("mySidenav").style.width = "280px";

}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("jobs__phone-criteriasCategory").style.width = "0";
    document.getElementById("jobs__phone-criteriasSubcategory").style.width = "0";
    document.getElementById("jobs__phone-criteriasEmploymentType").style.width = "0";
    document.getElementById("jobs__phone-criteriasExperience").style.width = "0";
    document.getElementById("jobs__phone-criteriasBackground").style.width = "0";
    document.getElementById("jobs__phone-criteriasLocation").style.width = "0";
    document.getElementById("jobs__phone-criteriasPosition").style.width = "0";
    document.getElementById("jobs__phone-criteriasAnnouncement").style.width = "0";
    document.getElementById("jobs__phone-criteriasDate").style.width = "0";
    document.getElementById("jobs__phone-criteriasRequired").style.width = "0";
    
   
    
}

function openNav1() {
    
    document.getElementById("jobs__phone-criteriasCategory").style.width = "280px";
}

function closeNav1() {

    document.getElementById("jobs__phone-criteriasCategory").style.width = "0";
}
function openNav2() {
    
    document.getElementById("jobs__phone-criteriasSubcategory").style.width = "280px";
}

function closeNav2() {

    document.getElementById("jobs__phone-criteriasSubcategory").style.width = "0";
}

function openNav3() {
    
    document.getElementById("jobs__phone-criteriasPosition").style.width = "280px";
}

function closeNav3() {

    document.getElementById("jobs__phone-criteriasPosition").style.width = "0";
}
function openNav5() {
    
    document.getElementById("jobs__phone-criteriasEmploymentType").style.width = "280px";
}

function closeNav5() {

    document.getElementById("jobs__phone-criteriasEmploymentType").style.width = "0";
}
function openNav6() {
    
    document.getElementById("jobs__phone-criteriasExperience").style.width = "280px";
}

function closeNav6() {

    document.getElementById("jobs__phone-criteriasExperience").style.width = "0";
}
function openNav7() {
    
    document.getElementById("jobs__phone-criteriasBackground").style.width = "280px";
}

function closeNav7() {

    document.getElementById("jobs__phone-criteriasBackground").style.width = "0";
}
function openNav8() {
    
    document.getElementById("jobs__phone-criteriasLocation").style.width = "280px";
}

function closeNav8() {

    document.getElementById("jobs__phone-criteriasLocation").style.width = "0";
}
function openNav9() {
    
    document.getElementById("jobs__phone-criteriasAnnouncement").style.width = "280px";
}

function closeNav9() {

    document.getElementById("jobs__phone-criteriasAnnouncement").style.width = "0";
}
function openNav10() {
    
    document.getElementById("jobs__phone-criteriasDate").style.width = "280px";
}

function closeNav10() {

    document.getElementById("jobs__phone-criteriasDate").style.width = "0";
}
function openNav11() {
    
    document.getElementById("jobs__phone-criteriasRequired").style.width = "280px";
}

function closeNav11() {

    document.getElementById("jobs__phone-criteriasRequired").style.width = "0";
}



$( "#addFilter" ).on( "click",  function() {
    $("#mySidenav").css({"width": "0"});
  });

//   $(document).ready(function(){
//     $("#addDate").click(function(){
    	
//         var today = new Date();
//         var dd = today.getDate();
//         var mm = today.getMonth() + 1; //January is 0!
//         var yyyy = today.getFullYear();

//         if (dd < 10) {
//         dd = '0' + dd;
//         }
//         if (mm < 10) {
//         mm = '0' + mm;
//         }
//         today = mm + '/' + dd + '/' + yyyy;
//     // document.write(today);
//         document.getElementById("date").innerHTML = "Last update :" +today;
// })	

// });
  
// $('i.like-unlike').on('click', function() {
//     $(this).toggleClass('flaticon-like flaticon-favorite-heart-button')
//   });
//login-register.html
//rotate form
// $(document).ready(function(){
//     $("#rotateToSignUP").click(function(){
//     	$(".login-register-card__side--front").attr('style', 'transform: rotateX(180deg)');
//     	$(".login-register-card__side--back").attr('style', 'transform: rotateX(0)');
//         $(".signInText").attr('style', 'display:none');
//         $(".signUpText").attr('style', 'display:block');
//         $("#rotateToSignUP1").addClass("d-none"); 
//         $(".get-password").attr('style', 'display: none');   
//         $(".register").attr('style', 'display: block');        
// })
//     $("#rotateToSignUP1").click(function(){
//         $(".login-register-card__side--front").attr('style', 'transform: rotateX(180deg)');
//         $(".login-register-card__side--back").attr('style', 'transform: rotateX(0)');
//         $(".signInText").attr('style', 'display:none');
//         $(".signUpText").attr('style', 'display:block');
//         $("#rotateToSignUP1").addClass('d-none');  
//         $(".get-password").attr('style', 'display: none');   
//         $(".register").attr('style', 'display: block');   
// })		
//     $("#rotateToLogIn").click(function(){
//         $(".login-register-card__side--front").attr('style', 'transform: rotateX(0)');
//         $(".login-register-card__side--back").attr('style', 'transform: rotateX(180deg)');
//         $(".signInText").attr('style', 'display:block');
//         $(".signUpText").attr('style', 'display:none');
//     	$("#rotateToSignUP1").removeClass("d-none");        
// })
// $(".forgot__pass").click(function(){
//     $(".login-register-card__side--front").attr('style', 'transform: rotateX(180deg)');
//     $(".login-register-card__side--back").attr('style', 'transform: rotateX(0)'); 
//     $(".register").attr('style', 'display: none');
//     $(".refresh-password").attr('style', 'display:none');
//     $(".get-password").attr('style', 'display: block');
         
// })
// $(".rotateToLogIn").click(function(){
//     $(".login-register-card__side--front").attr('style', 'transform: rotateX(0)');
//     $(".login-register-card__side--back").attr('style', 'transform: rotateX(180deg)');
//     $(".signInText").attr('style', 'display:block');
//     $(".signUpText").attr('style', 'display:none');
//     $("#rotateToSignUP1").removeClass("d-none");    
    
// })
// $(".request").click(function(){
//     $(".login-register-card__side--front").attr('style', 'transform: rotateX(180deg)');
//     $(".login-register-card__side--back").attr('style', 'transform: rotateX(0)'); 
//     $(".register").attr('style', 'display: none');
//     $(".refresh-password").attr('style', 'display:block');
//     $(".get-password").attr('style', 'display:none');
// })
// // addCV.html
// //  resume__header -rotate icon 
// $(".rotate-icon").click(function(){
//     $(this).toggleClass("down")  ; 
//    });
// });




// $(function() {
 
//     $("body").css({padding:0,margin:0});
//       var f = function() {
//         $(".content").css({position:"relative"});
//         var h1 = $("body").height();
//         var h2 = $(window).height();
//         var d = h2 - h1;
//         var h = $(".content").height() + d;    
//         var ruler = $("<div>").appendTo(".content");       
//         h = Math.max(ruler.position().top,h);
//         ruler.remove();    
//         $(".content").height(h);
//       };
//       setInterval(f,1000);
//       $(window).resize(f);
//       f();
     
//     });


//NAVBAR MOBILE VERSION
$( document ).ready(function() {
    $( "#openHeader" ).click(function() {
        $("#sideMenu").css("width", "80%");
        $("body").css("background-color", "rgba(0,0,0,0.5)");

      });
      $( "#closeHeader" ).click(function() {
        $("#sideMenu").css("width", "0");
        $("body").css("background-color", "unset");
      });
});

var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("nav__side--link-active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}




let selector = '.messages__list--box a';

$(selector).on('click', function(){
   
    $(this).addClass('message__active');
    $(".messages__list").css("display", "none");
    $(".messages__inner").css("display", "block");
});
$(document).on('click','.messages__back', function(){
    $('.messages__inner').css("display", "none");
    $('.messages__list').css("display", "block");
    $(selector).removeClass('message__active');

});


//NICE SELECT

$(document).ready(function() {
    $('.select-language').niceSelect();
});
//addcv.html

  $(document).ready(function(){
    $(".resume__contact-body").on("click","div", function (event) {   
      var content = $(this).parent().parent();
      var contentId = ('#' + $(content).attr('id'));
      var bodyId = contentId +'Body';
      var formId = contentId +'Form';
      $(bodyId).css("display","none");
      $(formId).css("display","block");
     
      $('.forMyResumeList').click(function(){
        $(this).data('clicked', true);
        $(bodyId).css("display","block");
        $(formId).css("display","none");
      });

    });
  });

  $(document).ready(function(){
    $('#resumeSection').click(function(){
      $('#mySidenav').css("width","250px");
    });
    $('#closeResumeSection').click(function(){
      $('#mySidenav').css("width","0");
    });
  });




  
// toggle password visibility



// POPUP

$(document).ready(function () {
  $("#popup__addResume--show").click(function () {
    $("#popup__addResume").show();
  });
  $("#popupClose").click(function () {
    $("#popup__addResume").hide();
  });
  $("#popup__addResume--cansel").click(function () {
    $("#popup__addResume").hide();
  });
  $("#popupCover--show").click(function () {
    $("#popupCover").show();
  });
  $("#popupCover--close").click(function () {
    $('#coverletterForm')[0].reset();
    $("#popupCover").hide();
  });
  $("#popupCover--cansel").click(function () {
    $('#coverletterForm')[0].reset();
    $("#popupCover").hide();
  });
  // $(document).on('click', ".myResume__addCoverLetter--listIcon",function(){
  //   var letterTitleText = $(this).parent().text();
  //   var letterTitle = $(this).parent();
  //   var letterContect = letterTitle.next().text();
  //   $("input[type=text]#cvName").val(letterTitleText);
  //   $("textarea[type=text]#my__textarea").val(letterContect);
  //   $("textarea#my__textarea").val(letterContect);
  //   $("#popupCover").show();
  // });
  $("#popupCover--close").click(function () {
    $('#coverletterForm')[0].reset();
    $("#popupCover").hide();
  });
  $("#popupApply__show").click(function () {
    $("#popupApply").show();
  });
  $("#popupApply--close").click(function () {
    $("#popupApply").hide();
  });
  $("#coverLetter__new").click(function () {
    $("#popupCover").show();
  });
  $("#remove__letter").click(function () {
    $("#popupRemoveLetter").show();
  });
  $(".popupRemoveLetter__close").click(function () {
    $("#popupRemoveLetter").hide();
  });
  $("#edit__letter").click(function () {
    $(".popupCover").show();
  });
  $("#editPopup__close").click(function () {
    $("popupCover").hide();
  });
  



  // rename cv
  // $(document).on('click', ".rename",function(){

  //   $(".ThertyPopup").fadeToggle( "slow", "linear" );
  //   var primaryDiv = $(this).closest( ".myResume__box" );
  //   var renameParagraf = $(primaryDiv).find(".myResume__title--name");

  //   $(".ThertyPopup__content--btn-link").click(function () {
  //     var inputVal =  $("input[type=text]#renameCv").val();
  //     $(renameParagraf).text(inputVal);
  //     $(".ThertyPopup").hide();
  //   });
  // });
  // $(".ThertyPopup__content--header-icon").click(function(){
  //   $(".ThertyPopup").hide();
  // });


  // remove cv

  // $(document).on('click', ".removeThisResume",function(){

  //   $(".SecondPopup").fadeToggle( "slow", "linear" );
  //   var primaryDiv = $(this).closest( ".myResume__box" );


  //   $(".removeResume").click(function () {

  //     $(primaryDiv).remove();
  //     $(".SecondPopup").hide();
  //   });
  //   $(".SecondPopup__content--header-icon").click(function(){
  //     $(".SecondPopup").hide();
  //   });
  //   $(".closeRemovePopup").click(function(){
  //     $(".SecondPopup").hide();
  //   });
  // });


});

//JOBS-SEARCH.HTML
//JS FOR RANGE(MIN -MAX SALARY)

// $(document).ready(function() {
//     $("#range").ionRangeSlider({
//         type: "double",
//         grid: true,
//         min: 0,
//         max: 10000,
//         from: 400,
//         to: 800,
//         step:50,
//         onFinish: function (data) {
//             // Called every time handle position is changed
//             // alert(data.from,data.to);
//             $('input[type=hidden].range-min-value').val(data.from);
//             $('input[type=hidden].range-max-value').val(data.to);
//         },
        
//     });
// });
// $('.owl-carousel').owlCarousel({
//     loop:true,
//     margin:10,
   
//     responsive:{
//         0:{
//             items:1
//         },
//         600:{
//             items:3
//         },
//         1000:{
//             items:3
//         }
//     }
// })

$( document ).ready(function() {
    $(".vacance__details--text dd,.vacance__details--text dd span,.vacance__details--text dd strong, .vacance__details--text dd strong span, .vacance__details--text dt,.vacance__details--text dt span,.vacance__details--text dt strong, .vacance__details--text dt strong span, .vacance__details--text p,.vacance__details--text p span, .vacance__details--text p strong, .vacance__details--text p strong span").each(function() {
        var $this = $(this);
        if($this.html().replace(/\s|&nbsp;/g, '','.').length <= 1)
            $this.remove();
    });
    $(".vacance__details--text dd,.vacance__details--text dd span,.vacance__details--text dd strong, .vacance__details--text dd strong span, .vacance__details--text dt,.vacance__details--text dt span,.vacance__details--text dt strong, .vacance__details--text dt strong span, .vacance__details--text p,.vacance__details--text p span, .vacance__details--text p strong, .vacance__details--text p strong span").each(function() {
        var $this = $(this);
        if($this.html().replace(/\s|&nbsp;/g, '','.').length <= 1)
            $this.remove();
    });
    $(".vacance__details--text dd,.vacance__details--text dd span,.vacance__details--text dd strong, .vacance__details--text dd strong span, .vacance__details--text dt,.vacance__details--text dt span,.vacance__details--text dt strong, .vacance__details--text dt strong span, .vacance__details--text p,.vacance__details--text p span, .vacance__details--text p strong, .vacance__details--text p strong span").each(function() {
        var $this = $(this);
        if($this.html().replace(/\s|&nbsp;/g, '','.').length <= 1)
            $this.remove();
    });
    $(".vacance__details--text dd,.vacance__details--text dd span,.vacance__details--text dd strong, .vacance__details--text dd strong span, .vacance__details--text dt,.vacance__details--text dt span,.vacance__details--text dt strong, .vacance__details--text dt strong span, .vacance__details--text p,.vacance__details--text p span, .vacance__details--text p strong, .vacance__details--text p strong span").each(function() {
        var $this = $(this);
        if($this.html().replace(/\s|&nbsp;/g, '','.').length <= 1)
            $this.remove();
    });
    $(".vacance__details--text dd,.vacance__details--text dt,.vacance__details--text p,.vacance__details--text span,.vacance__details--text strong, .vacance__details--text ul li, .vacance__details--text h1, .vacance__details--text h2, .vacance__details--text h3, .vacance__details--text h4, .vacance__details--text h5, .vacance__details--text h6").find("a,p,span,strong,h1,h2,h3,h4,h5,h6,dd,dt").contents().unwrap();
    $(".vacance__details--text dd,.vacance__details--text dt,.vacance__details--text p,.vacance__details--text span,.vacance__details--text strong, .vacance__details--text ul li,.vacance__details--text h1, .vacance__details--text h2, .vacance__details--text h3, .vacance__details--text h4, .vacance__details--text h5, .vacance__details--text h6").removeAttr("style");
    

});

// $( document ).ready(function() {
//     $(".select-language option[value='ru']").remove();
//     $(".select-language option").filter("[value='ru']").remove();
// });
//JOBS-SEARCH.HTML
//REMOVE <li class="jobs__criterias-list">
$(document).ready(function(){
    $(document).on('click','.delete-criterias',function(){
        var liElement = $(this).parent().parent();
        liElement.remove(); 
        var liElementId = liElement.attr("id");
        $("#defaultCheck" + liElementId).prop("checked", false);
    })
  });


 
  $(document).ready(function(){
	$("#toAdd").on("click","a", function (event) {
				var liElements = $(this).parent();
				var icon = $(liElements).children("i");
				icon.removeClass('flaticon-add').addClass('flaticon-tick');
        $("#myResume").append(liElements);
		//отменяем стандартную обработку нажатия по ссылке
				event.preventDefault();
		//забираем идентификатор бока с атрибута href
				var linkId = $(this).attr('href');
				var btnId = linkId +'Btn';
				
				var firstView = linkId +'FirstView';
				var form = linkId +'Form';
				var body = linkId +'Body';
				$(form).css("display","block");
				if ( $(linkId).is('.initiallyHidden') ) {
					//do something it does have the protected class!
					$(linkId).removeClass('d-none');
					$(body).addClass('d-none');
				}
				
				$(btnId).removeClass('d-none');
				$('.forMyResumeList').addClass('d-none');
					
				if ($(linkId).has( firstView )) {
					$(firstView).addClass('d-none');
				} 
								
				var linkId  = $(this).attr('href'),
			
		//узнаем высоту от начала страницы до блока на который ссылается якорь
				top = $(linkId).offset().top;
				 top = top - 100;
		//анимируем переход на расстояние - top за 1500 мс
		$('body,html').animate({scrollTop: top}, 1500);
		$(btnId).click(function(){
			$(this).data('clicked', true);
			
			icon.removeClass('flaticon-tick').addClass('flaticon-add');
			$("#toAdd").prepend(liElements);
			if ($(linkId).has( firstView )) {
				$(firstView).removeClass('d-none');
			} 
			if ( $(linkId).is('.initiallyHidden') ) {
				$(linkId).addClass('d-none');
				$(body).removeClass('d-none');
			}
			$(form).css("display","none");
		});
	});
});

 
$(document).ready(function(){
	$("#toAddd").on("click","a", function (event) {
				var liElementss = $(this).parent();
				var icons	=	$(liElementss).children("i");
				icons.removeClass('flaticon-add').addClass('flaticon-tick');
        $("#myResumee").append(liElementss);
		//отменяем стандартную обработку нажатия по ссылке
				event.preventDefault();
		//забираем идентификатор бока с атрибута href
				var linkIdd = $(this).attr('href');
				var btnIdd = linkIdd +'Btn';
				
				var firstVieww = linkIdd +'FirstView';
				var formm = linkIdd +'Form';
				var bodyy = linkIdd +'Body';
				$(formm).css("display","block");
				if ( $(linkIdd).is('.initiallyHidden') ) {
					//do something it does have the protected class!
					$(linkIdd).removeClass('d-none');
					$(bodyy).addClass('d-none');
				}
				
				$(btnIdd).removeClass('d-none');
				$('.forMyResumeList').addClass('d-none');
					
				if ($(linkIdd).has( firstVieww )) {
					$(firstVieww).addClass('d-none');
				} 
								
				var linkIdd  = $(this).attr('href'),
			
		//узнаем высоту от начала страницы до блока на который ссылается якорь
				topp = $(linkIdd).offset().top;
				 topp = topp - 100; 
		//анимируем переход на расстояние - top за 1500 мс
		$('body,html').animate({scrollTop: topp}, 1500);
		$(btnIdd).click(function(){
			$(this).data('clicked', true);
			
			icons.removeClass('flaticon-tick').addClass('flaticon-add');
			$("#toAdd").prepend(liElementss);
			if ($(linkIdd).has( firstVieww )) {
				$(firstVieww).removeClass('d-none');
			} 
			if ( $(linkIdd).is('.initiallyHidden') ) {
				$(linkIdd).addClass('d-none');
				$(bodyy).removeClass('d-none');
			}
			$(formm).css("display","none");
		});
	});

});


$(document).ready(function(){
	$("#myResume").on("click","a", function (event) {   
		//отменяем стандартную обработку нажатия по ссылке
				event.preventDefault();
		//забираем идентификатор бока с атрибута href
				var linkId  = $(this).attr('href'),
		//узнаем высоту от начала страницы до блока на который ссылается якорь
				top = $(linkId).offset().top;
				 top = top - 100;
		//анимируем переход на расстояние - top за 1500 мс
		$('body,html').animate({scrollTop: top}, 1500);
		
	});

});



$(document).ready(function() {

    $('.select-cv__carousel').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        dots:false,
        navText: [
            '<i class="flaticon-back" aria-hidden="true"></i>',
            '<i class="flaticon-right-arrow" aria-hidden="true"></i>'
        ],
        navContainer: '.select-cv__carouselBox .custom-nav',
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })


    $('.select-cv__carousel--1').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        dots:false,
        navText: [
            '<i class="flaticon-back" aria-hidden="true"></i>',
            '<i class="flaticon-right-arrow" aria-hidden="true"></i>'
        ],
        navContainer: '.select-cv__carouselBox--1 .custom-nav',
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })

  });




  $(document).ready(function() {

    $('.select-cv__carousel--1').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        dots:false,
        navText: [
            '<i class="flaticon-back" aria-hidden="true"></i>',
            '<i class="flaticon-right-arrow" aria-hidden="true"></i>'
        ],
        navContainer: '.select-cv__carouselBox--1 .custom-nav--1',
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })

  });

$(document).ready(function() {

    $('.myRadio-resume').click(function(){
        $('.myRadio-resume').removeClass("myRadio__active");
        $(this).addClass("myRadio__active");
    });

    $('.myRadio-cover').click(function(){
        $('.myRadio-cover').removeClass("myRadio__active");
        $(this).addClass("myRadio__active");
    });

  });

  $(document).ready(function() {
    //This condition will check if form with id 'select-cv__form' is exist then only form reset code will execute.
    if($('#select-cv__form').length>0){
        $('#select-cv__form')[0].reset(); 
    }
});
// $(document).ready(function() {
   
//     $(".js-example-placeholder-single").select2({
//         placeholder: "Country",
//         allowClear: true,
//         tags: ['a', 'b', 'c'],
        
//         escapeMarkup : function(markup) { return markup; }
//     });
//     $(".js-example-placeholder-single1").select2({
//         placeholder: "Sity",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single2").select2({
//         placeholder: "Nationality",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single3").select2({
//         placeholder: "Salary",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single4").select2({
//         placeholder: "Driver's license",
//         allowClear: true,
//         tags: true
//     });
//     $(".select5").select2({
//         placeholder: "Military service",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single6").select2({
//         placeholder: "Category",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single7").select2({
//         placeholder: "Percent",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single8").select2({
//         placeholder: "Company name",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single9").select2({
//         placeholder: "Title",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single10").select2({
//         placeholder: "Company sector",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single11").select2({
//         placeholder: "Business Field ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single12").select2({
//         placeholder: "Job type ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single13").select2({
//         placeholder: "Sity ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single14").select2({
//         placeholder: "Education level ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single15").select2({
//         placeholder: "Graduation Degree Max* ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single16").select2({
//         placeholder: "Graduation Degree ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single17").select2({
//         placeholder: "University* ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single18").select2({
//         placeholder: "Faculty ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single19").select2({
//         placeholder: "Department ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single20").select2({
//         placeholder: "Education type* ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single21").select2({
//         placeholder: "Education Language ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single22").select2({
//         placeholder: "Scholarship Type*",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single23").select2({
//         placeholder: "Scholarship Rate ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single24").select2({
//         placeholder: "Minor Program* ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single25").select2({
//         placeholder: "Graduation Degree* ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single26").select2({
//         placeholder: "Double Major Faculty* ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single27").select2({
//         placeholder: "Minor Program* ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single28").select2({
//         placeholder: "Graduation Degree* ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-basic-multiple").select2({
//         placeholder: "You can edit your skills in your resume. ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single29").select2({
//         placeholder: "Language* ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single30").select2({
//         placeholder: "Level* ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single31").select2({
//         placeholder: "Exam name ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single32").select2({
//         placeholder: "Certificates name ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single33").select2({
//         placeholder: "Reference type ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single34").select2({
//         placeholder: "Language ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single35").select2({
//         placeholder: "Title ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single36").select2({
//         placeholder: "Company Sector ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single37").select2({
//         placeholder: "High school type ",
//         allowClear: true,
//         tags: true
//     });
//     $(".js-example-placeholder-single38").select2({
//         placeholder: "High school departament ",
//         allowClear: true,
//         tags: true
//     });
//     $(".popup__select1").select2({
//         placeholder: "Choose CV ",
//         allowClear: true,
        
//     });
//     $(".popup__select2").select2({
//         placeholder: "Choose cover letter",
//         allowClear: true,
        
//     });
//     $(".salary").select2({
//         placeholder: "Choose salary",
//         allowClear: true,
        
//     });
//     $(".select-resume").select2({
//         placeholder: "Select your CV ",
//         allowClear: true,
//         tags: true
//     });
//     $(".cv").select2({
//         placeholder: "Select",
//         allowClear: true,
//     });
//     $(".applications").select2({
//         placeholder: "Select",
//         allowClear: true,
//     });
    
// });


$(document).ready(function(){
    $(".js-example-placeholder-single22").change(function(){
        if ($(".js-example-placeholder-single22").val() === "noScholarship") {
            $(".scholarshipRate").attr('style', 'display:none');
        } else  {
            $(".scholarshipRate").attr('style', 'display:block');
           
          }
    })

    $(".js-example-placeholder-single14").change(function(){
        if ($(".js-example-placeholder-single14").val() === "highSchool") {
            $(".highSchool").addClass("d-none");
            $(".highSchoolElements").removeClass("d-none");
        } else  {
            $(".highSchool").removeClass("d-none");
            $(".highSchoolElements").addClass("d-none");
          }
    })
    // $(".popup__select2").change(function(){
    //     if ($(".popup__select2").val() === "newCoverLetter") {
    //         $("#popupCoverLetter").show();
    //         $("#popupCoverLetter--close").click(function(){
    //             $("#popupCoverLetter").hide();
    //           });
           
    //     } else  {
    //         $(".highSchool").removeClass("d-none");
    //         $(".highSchoolElements").addClass("d-none");
    //       }
    // })
    
    $(".select5").change(function(){
        if ($(".select5").val() === "Exempt") {
            $(".exempt").removeClass("d-none");
        } else  {
            $(".exempt").addClass("d-none");
          }
    })
    $(".select5").change(function(){
        if ($(".select5").val() === "Postponed") {
            $(".postponed").removeClass("d-none");
        } else  {
            $(".postponed").addClass("d-none");
          }
    })
})




$(document).ready(function(){

    $('select').change(function(){
        var select = $(this)
        var selectId = select.attr('id');
        var label = $("label[for='"+selectId+"']");
        console.log(label);
        if (select.val() == '') {
            
            label.css({"display": "none"});
        }
        else{
            label.css({"display": "block"});
        }
    }); 



    // $("select").change(function(){
    //     var selectId = $(this).attr("id")
    //     var label = selectId.prev();
    //     console.log(label);
    //     console.log(mthis);
    //     if($(this).val() == "")
    //          label.css({"display": "none"});
    //     else
    //          label.css({"display": "block"});
    // })
})


// $("#candidateBirthdayInput").flatpickr({
//     enableTime: false,
//     dateFormat: "Y-m-d"
// });

// $("#candidateDateOfDelayInput").flatpickr({
//     enableTime: false,
//     dateFormat: "Y-m-d"
// });

//TABLE

$(document).ready(function() {
    var table = $('#example').DataTable({
        // "paging":   false,        
        "info":     false,
        "scrollX": true,
        "language": {
            "search": ""
          },
        
        // initComplete : function() {
        //     var input = $('.dataTables_filter input').unbind(),
        //         self = this.api(),
        //         $searchButton = $('<button class="flaticon-magnifying-glass icon"></button>')
        //                 //    .text('search')
        //                    .click(function() {
        //                       self.search(input.val()).draw();
        //                    })
               
        //     $('.dataTables_filter').append($searchButton);
        // }            
    }) 
} );


 
$(document).ready(function () {
    // This WILL work because we are listening on the 'document', 
    // for a click on an element with an ID of #test-element
    $(".form__customInput").on("change", function () {
        let fileName = $(this).val().split("\\").pop();
        let parentInp = $(this).parent();
        parentInp.find( ".form__customInpTitle" ).addClass("form__customInpTitle--label");
        parentInp.find( ".form__customLabel" ).css('color', '#686868');

        $(this).siblings(".form__customLabel").addClass("selected").text(fileName);

    });

    $(document).on("click", ".clearValue", function () {
        let parent = $(this).parent();
        let placeholder = parent.find('.form__customInpTitle').text();
        console.log(placeholder);
        parent.find('.form__customInput').val('');
        parent.find('.form__customInpTitle').removeClass("form__customInpTitle--label");
        parent.find( ".form__customLabel" ).css('color', 'transparent');
        parent.find('.form__customLabel').removeClass("selected").text(placeholder);
    });

});
$(document).ready(function () {
    $(".account__edit-link").click(function () {
        $(this).parent().css("display", "none")
                .next().css("display", "block");
    });

    $(".account__cancel").click(function () {
        $(this).parent().parent().css("display", "none")
            .prev().css("display", "flex");
    });

    $(".account__update").click(function () {
        $(this).parent().parent().css("display", "none")
            .prev().css("display", "flex");
    });

    // $(".account__update--1").click(function(){
    //     if($(".account__edit--surname").val() != ""){
    //         $(".account__info--surname").text($(".account__edit--surname").val())
    //     }
    //     else{
    //         $(".account__info--surname").text();
    //     }
    //     if($(".account__edit--name").val() != ""){
    //         $(".account__info--name").text($(".account__edit--name").val())
    //     }
    //     else{
    //         $(".account__info--name").text();
    //     }
    // });

    // $(".account__update--2").click(function(){
    //     if($(".account__edit--email")){
    //         $(".account__info--email").text($(".account__edit--email").val());
    //     }
    // });
});
$(document).ready(function () {

    
  
});
$(document).ready(function(){

    // Initialize Select2
    $('#sel_users').select2();
  
    // Set option selected onchange
    $('#selectCV').change(function(){
      
        let selectResume = $(this).val();
        if (selectResume != 0){
            $('.viewCV').css("display", "block");
        }
        else{
            $('.viewCV').css("display", "none");
        }
  
    });
  });